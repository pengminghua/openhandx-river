
CREATE TABLE `riv_loginuser` (
  `NAME` varchar(16) NOT NULL,
  `PASSWORD` varchar(36) NOT NULL,
  PRIMARY KEY  (`NAME`),
  UNIQUE KEY `IDX_USERGROUP_NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `riv_loginuser` VALUES ('root','96E79218965EB72C92A549DD5A330112');


CREATE TABLE `riv_marketservice` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(32) NOT NULL,
  `CHARTSET` varchar(16) default NULL,
  `USER` varchar(16) NOT NULL,
  `PASSWORD` varchar(16) NOT NULL,
  `SERVICEURLS` varchar(2048) NOT NULL,
  `TIMEOUT` int(11) default NULL,
  `TRYDEGREE` int(11) default NULL,
  `TRYTIMES` varchar(256) default NULL,
  `DESCRIPTION` varchar(1024) default NULL,
  `PROXYHOST` varchar(256) default NULL,
  `PROXYPORT` int(11) default NULL,
  `PROXYUSER` varchar(16) default NULL,
  `PROXYPASSWORD` varchar(16) default NULL,
  `USED` int(11) NOT NULL,
  `LASTTIME` datetime NOT NULL,
  `SERVICE` varchar(64) default NULL,
  PRIMARY KEY  (`ID`),
  UNIQUE KEY `IDX_MARKETSERVICE_NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `riv_messageaccredit` (
  `MESSAGEID` int(11) NOT NULL,
  `USERGROUPID` int(11) NOT NULL,
  PRIMARY KEY  (`MESSAGEID`,`USERGROUPID`),
  UNIQUE KEY `IDX_MESSAGEACCREDIT` (`USERGROUPID`,`MESSAGEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `riv_messagename` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(32) NOT NULL,
  `MAXCOUNT` int(11) default NULL,
  `DESCRIPTION` varchar(2048) default NULL,
  PRIMARY KEY  (`ID`),
  UNIQUE KEY `IDX_MESSAGE_NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `riv_serviceaccredit` (
  `MARKETSERVICEID` int(11) NOT NULL,
  `USERGROUPID` int(11) NOT NULL,
  PRIMARY KEY  (`MARKETSERVICEID`,`USERGROUPID`),
  UNIQUE KEY `IDX_ACCREDIT` (`USERGROUPID`,`MARKETSERVICEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `riv_usergroup` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(32) NOT NULL,
  `PASSWORD` varchar(36) default NULL,
  `DESCRIPTION` varchar(1024) default NULL,
  `GROUPFLAG` int(11) NOT NULL,
  `USED` int(11) NOT NULL,
  PRIMARY KEY  (`ID`),
  UNIQUE KEY `IDX_USERGROUP_NAME` (`NAME`,`GROUPFLAG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `riv_usergrouprela` (
  `USERID` int(11) NOT NULL,
  `GROUPID` int(11) NOT NULL,
  PRIMARY KEY  (`USERID`,`GROUPID`),
  UNIQUE KEY `IDX_USERGROUPRELA` (`GROUPID`,`USERID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

